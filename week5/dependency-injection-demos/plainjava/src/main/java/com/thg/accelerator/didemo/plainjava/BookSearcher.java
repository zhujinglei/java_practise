package com.thg.accelerator.didemo.plainjava;

import com.thg.accelerator.didemo.plainjava.loader.BookLoader;
import com.thg.accelerator.didemo.plainjava.repository.BookRepository;
import com.thg.accelerator.didemo.plainjava.repository.RepositoryException;

import java.util.List;

public class BookSearcher {
  private BookLoader bookLoader;
  private BookRepository bookRepository;

  public BookSearcher(BookLoader bookLoader, BookRepository bookRepository) {
    this.bookLoader = bookLoader;
    this.bookRepository = bookRepository;
    initRepository();
  }

  public List<Book> findBooks(String query) throws RepositoryException {
    return bookRepository.findBooks(query);
  }

  private void initRepository() {
    bookLoader.load().stream().forEach(book -> {
      try {
        bookRepository.createBook(book);
      } catch (RepositoryException e) {
        throw new RuntimeException("Unable to save books :" , e);
      }
    });
  }
}
