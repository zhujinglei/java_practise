package com.thg.accelerator.didemo.plainjava.repository.impl;

import com.thg.accelerator.didemo.plainjava.Book;
import com.thg.accelerator.didemo.plainjava.repository.BookRepository;
import com.thg.accelerator.didemo.plainjava.repository.RepositoryException;

import java.util.List;
import java.util.Optional;

public class SqlLiteBookRepository implements BookRepository {
  @Override
  public void createBook(Book book) throws RepositoryException {

  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    return Optional.empty();
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
    return null;
  }
}
