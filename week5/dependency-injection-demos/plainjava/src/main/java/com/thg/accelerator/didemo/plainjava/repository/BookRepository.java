package com.thg.accelerator.didemo.plainjava.repository;

import com.thg.accelerator.didemo.plainjava.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {
  void createBook(Book book) throws RepositoryException;
  void updateBook(Book book) throws RepositoryException;
  void deleteBook(int isbn) throws RepositoryException;
  Optional<Book> getBook(int isbn) throws RepositoryException;
  List<Book> findBooks(String search) throws RepositoryException;
}
