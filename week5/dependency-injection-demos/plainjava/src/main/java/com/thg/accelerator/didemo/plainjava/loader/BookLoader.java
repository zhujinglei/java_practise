package com.thg.accelerator.didemo.plainjava.loader;

import com.thg.accelerator.didemo.plainjava.Book;

import java.util.List;

public interface BookLoader {
  List<Book> load();
}
