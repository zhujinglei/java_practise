package com.thg.accelerator.didemo.springxml.repository;

public class RepositoryException extends Exception {
  public RepositoryException(String message) {
    super(message);
  }

  public RepositoryException(String message, Throwable cause) {
    super(message, cause);
  }
}
