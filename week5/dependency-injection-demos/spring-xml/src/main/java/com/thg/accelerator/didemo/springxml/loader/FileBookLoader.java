package com.thg.accelerator.didemo.springxml.loader;

import com.thg.accelerator.didemo.springxml.Book;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FileBookLoader implements BookLoader {
  private String filename;

  public FileBookLoader(String filename) {
    this.filename = filename;
  }

  @Override
  public List<Book> load() {
    LinkedList<Book> books = new LinkedList<>();
    try {
      BufferedReader reader = new BufferedReader(new FileReader(filename));
      String line;
      line = reader.readLine();
      while (line != null) {
        String[] split = line.split(";");
        Book book = new Book.BookBuilder().author(split[1])
            .title(split[0])
            .isbn(Long.parseLong(split[2]))
            .synopsis(split[3])
            .createBook();
        books.add(book);
        line = reader.readLine();
      }
    } catch (IOException e) {
      return books;
    }

    return books;
  }
}
