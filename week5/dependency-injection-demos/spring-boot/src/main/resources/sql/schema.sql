create database library;
\connect library;

CREATE TABLE books (
 isbn BIGINT PRIMARY KEY,
 author VARCHAR (50) NOT NULL,
 synopsis VARCHAR (5000) NOT NULL,
 title VARCHAR (355) NOT NULL
);