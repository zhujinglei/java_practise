package com.thg.accelerator.didemo.springxml.repository.impl;

import com.thg.accelerator.didemo.springxml.Book;
import com.thg.accelerator.didemo.springxml.repository.BookRepository;
import com.thg.accelerator.didemo.springxml.repository.RepositoryException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class InMemoryBookRepository implements BookRepository {
  private Map<Long, Book> library;

  public InMemoryBookRepository() {
    this.library = new HashMap<>();
  }

  @Override
  public void createBook(Book book) throws RepositoryException {
    if (library.containsKey(book.getIsbn())) {
      throw new RepositoryException(String.format("A book with isbn: %d already exists", book.getIsbn()));
    } else {
      library.put(book.getIsbn(), book);
    }
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {
    if (!library.containsKey(book.getIsbn())) {
      throw new RepositoryException(String.format("A book with isbn: %d does not exist", book.getIsbn()));
    } else {
      library.put(book.getIsbn(), book);
    }
  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {
    if (!library.containsKey(isbn)) {
      throw new RepositoryException(String.format("A book with isbn: %d does not exist", isbn));
    } else {
      library.remove(isbn);
    }
  }

  @Override
  public Optional<Book> getBook(int isbn) {
    if (library.containsKey(isbn)) {
      return Optional.of(library.get(isbn));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public List<Book> findBooks(String search) {
    return library.entrySet().stream().map(Map.Entry::getValue)
        .filter(b -> b.getSynopsis().contains(search)
            || b.getAuthor().contains(search))
                  .collect(Collectors.toList());
  }
}
