package com.thg.accelerator.didemo.springxml;

import com.thg.accelerator.didemo.springxml.loader.BookLoader;
import com.thg.accelerator.didemo.springxml.repository.BookRepository;
import com.thg.accelerator.didemo.springxml.repository.RepositoryException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookSearcher implements InitializingBean {
  @Autowired
  private BookLoader bookLoader;
  @Autowired
  private BookRepository bookRepository;

  public List<Book> findBooks(String query) throws RepositoryException {
    return bookRepository.findBooks(query);
  }

  @Override
  public void afterPropertiesSet() {
    bookLoader.load().stream().forEach(book -> {
      try {
        bookRepository.createBook(book);
      } catch (RepositoryException e) {
        throw new RuntimeException("Unable to save books :" , e);
      }
    });
  }
}
