package com.thg.accelerator.didemo.springxml.repository.impl;

import com.thg.accelerator.didemo.springxml.Book;
import com.thg.accelerator.didemo.springxml.repository.BookRepository;
import com.thg.accelerator.didemo.springxml.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PostgresBookRepository implements BookRepository {
  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;


  @Override
  public void createBook(Book book) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
                                      .addValue("isbn", book.getIsbn())
                                      .addValue("title", book.getTitle())
                                      .addValue("author", book.getAuthor())
                                      .addValue("synopsis", book.getSynopsis());
    try {
      jdbcTemplate.update("insert into books (isbn, title, author, synopsis) values (:isbn, :title, :author, :synopsis)", namedParameters);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    return Optional.empty();
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
    jdbcTemplate.query()
    return null;
  }
}
