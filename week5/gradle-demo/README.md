# Installing gradle
## Linux
Follow the instructions [here](https://linuxize.com/post/how-to-install-gradle-on-ubuntu-18-04/). Make sure you replace the version in the article with the latest version listed [here](https://gradle.org/releases/)

## MacOS
* Download complete version of v5.1.1 from https://gradle.org/releases/
* Unzip and move the unzipped folder to Applications
* Open your bash_profile file and export the path to folder like this:
  ```export PATH=/Applications/gradle-5.1.1/bin:$PATH```
* Close your terminal and in a newly opened one type gradle -v
* You will see the gradle version with notes

## Initialising a (Java) gradle project
Run the command from inside the folder containing your soon to be gradle project: ```gradle init --type java-application```. 

Read about what this does [here](https://guides.gradle.org/creating-new-gradle-builds/)

Check out the available tasks by running ```./gradlew tasks```

## What's the gradle wrapper?
The gradle wrapper is a utility to make sure all developers on your project are building with the same gradle version. 
Read more about this [here](https://docs.gradle.org/5.1.1/userguide/gradle_wrapper.html). You should use ```gradlew``` from now on to build your project.

## Reading about source sets
Gradle allows you to have whatever folder structure you want. Read more about this [here](https://docs.gradle.org/current/userguide/java_plugin.html#source_sets).
I've defined a ```lol``` source set, which can be compiled and ran independently from the ```main``` source set.



## Some useful tasks
```gradlew build```

```gradlew run```

```gradlew clean```

```gradlew test```

```gradlew jar```

The task below is a custom task defined in ```build.gradle```:

```gradlew sayHello```
