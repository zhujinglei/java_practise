package weekexercise;

import java.util.ArrayList;
import java.util.List;

public class DisplayInfomation {

  private List<EmployeeInformation> employeelist = new ArrayList<>();

  public DisplayInfomation(List<EmployeeInformation> employeelist) {
    this.employeelist = employeelist;
  }

  public List<EmployeeInformation> getEmployeelist() {
    return employeelist;
  }

  public void DiplaytheInfo() {

    System.out.println("Here is the employee information");
    System.out.println(" ");
    for (EmployeeInformation employeeInformation : getEmployeelist()) {
      System.out.println(employeeInformation.getFirstname() + "/t" + employeeInformation.getSurname() + "/t" + employeeInformation.getSalary());
    }

  }


}
