package weekexercise;

import java.util.ArrayList;
import java.util.List;

public class PromotionSalary {
  private List<EmployeeInformation> employeeInformationList = new ArrayList<>();
  private double promotionPercentageincrease;

  public PromotionSalary(List<EmployeeInformation> employeeInformations, double promotionPercentageincrease) {
    this.employeeInformationList = employeeInformations;
    this.promotionPercentageincrease = promotionPercentageincrease;
  }

  public List<EmployeeInformation> getEmployeeInformations() {
    return employeeInformationList;
  }

  public double getPromotionPercentageincrease() {
    return promotionPercentageincrease;
  }

  public void setPromotionPercentageincrease(double promotionPercentageincrease) {
    this.promotionPercentageincrease = promotionPercentageincrease;
  }

  public double increaseSalaryofPromotion(){

    return employeeInformationList.stream().mapToDouble(e->(e.getFirstname().charAt(0)< 'n'? getPromotionPercentageincrease():1.0)
        *e.getSalary()).sum();

  }

}
