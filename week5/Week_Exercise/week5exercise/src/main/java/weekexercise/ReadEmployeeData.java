package weekexercise;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

// generic means parametriezed type;  Paramterized types are important because they enable you to create
// classes; interface and methods in which the type of data upon which they operat is specified as a parameter;

public class ReadEmployeeData {

  private String filepath;

  List<EmployeeInformation> employeelist = new ArrayList<>();

  public ReadEmployeeData(String filepath) {
    this.filepath = filepath;
  }

  public String getFilepath() {
    return filepath;
  }

  public void setFilepath(String filepath) {
    this.filepath = filepath;
  }

  public void LoadData() {

    BufferedReader reader;

    try {
      reader = new BufferedReader(new FileReader(getFilepath()));
      reader.readLine();
      String line = reader.readLine();
      while (line != null) {
        String[] data = line.split("\\|");
        EmployeeInformation EmployeeInfo = new EmployeeInformation(data[0], data[1], Double.parseDouble(data[2]));
        employeelist.add(EmployeeInfo);
        line = reader.readLine();
      }
    } catch (Exception e) {
      System.out.println("Can not load the file: please check the file Path " + getFilepath());
    }
  }
}
