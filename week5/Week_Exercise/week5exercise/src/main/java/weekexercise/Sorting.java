package weekexercise;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Sorting {

  private List<EmployeeInformation> employeelist = new ArrayList<>();

  public Sorting(List<EmployeeInformation> employeelist) {
    this.employeelist = employeelist;
  }

  public List<EmployeeInformation> getEmployeelist() {
    return employeelist;
  }

  public void sortByFirstNameAec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getFirstname));
  }

  public void sortBySurnameAec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getSurname));
  }

  public void sortSalaryAec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getSalary));

  }

  public void sortByFirstNameDec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getFirstname).reversed());
  }

  public void sortBySurnameDec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getSurname).reversed());
  }

  public void sortSalaryDec() {
    employeelist.sort(Comparator.comparing(EmployeeInformation::getSalary).reversed());
  }

}
