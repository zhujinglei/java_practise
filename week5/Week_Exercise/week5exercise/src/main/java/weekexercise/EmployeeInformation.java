package weekexercise;

public class EmployeeInformation {

  private String firstname;
  private String surname;
  private double salary;


  public EmployeeInformation(String firstname, String surname, double salary) {
    this.firstname = firstname;
    this.surname = surname;
    this.salary = salary;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getSurname() {
    return surname;
  }

  public double getSalary() {
    return salary;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }
}
