package com.thg.accelerator;

import java.util.ArrayList;

public interface UpdateServer {

    public double updateVersion(double value);
}
