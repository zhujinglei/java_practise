package com.thg.accelerator;

public interface ServerRepository {

    public String getData(String urlString);
}
