package com.thg.accelerator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class PostDataTest {
  // create object;

  PostData postData = new PostData();

  @Before
  public void setUp() {
    // whats' the meaning of the UpdatedServer.class mean?
    postData.updateMyServer = Mockito.mock(UpdateServer.class);
    Mockito.when(postData.updateMyServer.updateVersion(0.99999999)).thenReturn(1.0);
  }

  @Test
  public void updateRepositoryVersion() {

    assertTrue("Upgrade version from 0.9999999 to 1.0 ", 1.0==postData.updateRepositoryVersion(0.99999999));
    assertFalse("Updating fail from 0.99999999 to 1.0", 0.99999999 ==postData.updateRepositoryVersion(0.99999999));
  }
}