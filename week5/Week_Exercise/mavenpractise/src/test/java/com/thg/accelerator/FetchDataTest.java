package com.thg.accelerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;

import org.mockito.Mockito;


public class FetchDataTest {

  FetchData fetchData = new FetchData();

  @Before
  public void setUp() {
    fetchData.myRepo = Mockito.mock(ServerRepository.class);
    Mockito.when(fetchData.myRepo.getData("www.thg.com/datapost")).thenReturn("hihi");
  }


  @Test
  public void hitServerRepository() {


    assertEquals("Fetch your data from our website", "hihi", fetchData.hitServerRepository());
  }
}