## Setting up a jgitflow workflow in a maven project
### Prerequisites
Make sure your maven is configured correctly to point to the THG artifactory -- instructions [here](https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/blob/master/week2/test_demo/README.md)

### Configuring your global environment
Run this command to add the RSA public key of gitlab into your known_hosts

```ssh-keyscan -t RSA -H gitlab.io.thehut.local >> ~/.ssh/known_hosts```

### Configuring your project
Add the following to your plugins:

```xml        
<plugin>
 <groupId>external.atlassian.jgitflow</groupId>
 <artifactId>jgitflow-maven-plugin</artifactId>
 <version>1.0-m5.1</version>
 <configuration>
   <pushReleases>true</pushReleases>
   <pushHotfixes>true</pushHotfixes>
   <allowUntracked>true</allowUntracked>
 </configuration>
</plugin>
```

Add the following to the root level of your pom (a child of the project element):

```xml

  <scm>
    <connection>scm:git:git@gitlab.io.thehut.local:halls/connect-n-template-player.git</connection>
    <developerConnection>scm:git:git@gitlab.io.thehut.local:halls/connect-n-template-player.git</developerConnection>
  </scm>
```

Make sure you change the git references to your project.

### Performing a release

From your develop branch, make sure you have a clean working tree (all changes committed and pushed)

```mvn jgitflow:release-start jgitflow:release-finish ```

Accept the defaults, then run the following command to push the tag

```git push --tags```

           
