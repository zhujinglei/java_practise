package InheritanceDemo;

public class Shapes {

    public static void main(String args[]){

        Triangle t1 = new Triangle();
        Triangle t2 = new Triangle();

        t1.width = 4.0;
        t1.height = 4.0;
        t1.style = "Filled";

        t2.width = 8.0;
        t2.height = 12.0;
        t2.style = "Outlined";

        System.out.println("Info for t1 is ");

        t1.showDim();
        t1.showStyle();

        System.out.println("Area is " + t1.area());


        System.out.println("Info for t2 is ");

        t2.showDim();
        t2.showStyle();

        System.out.println("Area is " + t2.area());

    }

}
