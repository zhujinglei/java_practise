package InheritanceDemo.AccessorDemo;

public class MainDemo {

    public static void main(String args[]){

        Triangle t1=new Triangle();

        t1.setHeight(10.0);
        t1.setWidth(20.0);
        t1.style="good one";

        System.out.println("Info for this shape is ");
        t1.showStyle();
        t1.showInfo();
        System.out.println("The area of this shape is " + t1.area());
    }
}
