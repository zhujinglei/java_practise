package InheritanceDemo.AccessorDemo;

public class TwoDShape {
    private double width;
    private double height;

    double getWidth(){return width ;}
    double getHeight(){return height;}

    void setWidth(double W){
        width = W;
    }

    void setHeight(double H){
        height= H;
    }

    void showInfo(){
        System.out.println("The shape is width and height " + width +" " + height);
    }
}

