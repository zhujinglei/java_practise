public class VehicleDemo {
    public static void main(String args[]) {

        Vehicle minivan = new Vehicle();
        Vehicle sportscar = new Vehicle();

        int range1, range2;

        minivan.fuelcap=16;
        minivan.passengers=7;
        minivan.mpg=21;

        sportscar.fuelcap=14;
        sportscar.passengers=2;
        sportscar.mpg=12;

        range1= minivan.fuelcap*minivan.mpg;
        range2= sportscar.fuelcap*sportscar.mpg;

        System.out.println("Minivan carry passengers " + minivan.passengers+ " in " + range1 );
        minivan.range();
        System.out.println("Sportscar carry passengers " + sportscar.passengers+ " in " + range2 );
        sportscar.range();
    }
}
