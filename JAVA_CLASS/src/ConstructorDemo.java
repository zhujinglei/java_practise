// a constructor initializes an object when it is created. It has the same name as its class/

class ConstructorDemo {
    int passengers;
    int fuelcap;
    int mpg;

    ConstructorDemo(int p, int f, int m){
        passengers = p;
        fuelcap = f;
        mpg = m;
    }

    double fuelneeded (int mile){

        return(double) miles / mpg;

    }
}


