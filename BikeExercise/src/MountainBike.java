//method overriding is used to change impletation of a method in a child; overriding always occurrs in a
// child class, parameter must be the same;
// Method overriding is the example of run time polymophism.

public class MountainBike extends Bike {
    private int yearsOfExtendedWarranty;

    public MountainBike() {
        super();
        this.yearsOfExtendedWarranty = yearsOfExtendedWarranty;
    }
    public void bikeDescription (){
        System.out.println(" This bike Comes with 10 years of warranty ");
    }
    // example of method overriding
}

