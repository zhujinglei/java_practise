//method overloading is performed within class;

public class Bike {
    private int bikeNumber;
    private String bikeColour;
    private int yearsOfStandardWarranty;

    public Bike() {
        this.bikeNumber = 1;
        this.bikeColour = "Red";
        this.yearsOfStandardWarranty = 5;

    }

    //example of overloaded constructor;
    public Bike(int bikeNum, String bikeClr, int standardWarrant) {
        bikeNumber = bikeNum;
        bikeColour = bikeClr;
        yearsOfStandardWarranty = standardWarrant;

    }

    public void bikeDescription(){
        System.out.println("This Bike Comes with  5 years of warranty");
    }

}
