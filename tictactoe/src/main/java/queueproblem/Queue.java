//package queueproblem;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//public class Queue <T>{
//  private List<PriorityElement> list = new ArrayList<>();
//
//
//  public Queue(){
//  }
//
//  public void add(T element, double priorityValue){
//    PriorityElement pe = new PriorityElement<>(element, priorityValue);
//    list.add(pe);
//    Collections.sort(list, new Compar());
//  }
//
//  public PriorityElement<T> peek(){
//    if (list.isEmpty()){
//      System.out.println("The queue is empty");
//      return null;
//    } else {
//      return list.get(0);
//    }
//  }
//
//  public PriorityGeneric<T> poll(){
//    if (list.isEmpty()){
//      System.out.println("The queue is empty");
//      return null;
//    }
//    return list.remove(0);
//  }
//
//  class Compar implements Comparator<PriorityGeneric>
//  {
//    // Used for sorting in ascending order of
//    // roll number
//    public int compare(PriorityElement a, PriorityElement b)
//    {
//      if (a.getPriority() > b.getPriority()) return 1;
//      if (a.getPriority() == b.getPriority()) return 0;
//      return -1;
//    }
//  }
//}
