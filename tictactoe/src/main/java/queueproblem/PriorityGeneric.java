package queueproblem;

public class PriorityGeneric<T> {
  private T element;

  public T getElement() {
    return element;
  }

  public double getPriority() {
    return priority;
  }

  private double priority;
  public PriorityGeneric(T originElement, double priority) {
    this.element = originElement;
    this.priority = priority;
  }
}
