package hashpassword;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HashingPassword {
  private static String FNVHash1(String data) {
    final int p = 16777619;
    int hash = (int) 2166136261L;
    for (int i = 0; i < data.length(); i++) {
      hash = (hash ^ data.charAt(i)) * p;
    }
    hash += hash << 13;
    hash ^= hash >> 7;
    hash += hash << 3;
    hash ^= hash >> 17;
    hash += hash << 5;
    return Integer.toHexString(hash);
  }

  private static String getRandomString(int length) {
    String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    Random random = new Random();
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < length; i++) {
      int number = random.nextInt(62);
      sb.append(str.charAt(number));
    }
    return sb.toString();
  }

  public static String getHash(String cleartext) {
    int saltLength = 10;
    String saltString = getRandomString(saltLength);
    return getSaltHast(cleartext, saltString);
  }

  private static String getSaltHast(String cleartext, String salt) {
    String hashString = FNVHash1(cleartext + salt);
    return "$" + salt + "$" + hashString;
  }


  public static boolean verify(String hashCode, String cleartext) {
    Pattern pattern = Pattern.compile("\\$(\\S+)\\$");
    Matcher matcher = pattern.matcher(hashCode);
    if (matcher.find()) {
      String salt = matcher.group(1);
      String newHashCode = getSaltHast(cleartext, salt);
      return hashCode.equals(newHashCode);
    }
    return false;
  }


}

