package tictactoe.display;

import java.awt.*;

public class BoardDisplay implements Display {
  @Override
  public void display(char[][] board) {
    for (int i = 0; i <= board.length - 1; i++) {
      System.out.println("----------------------");
      for (int m = 0; m <= board[i].length - 1; m++) {
        System.out.print(" | ");
        System.out.print(board[i][m]);
        System.out.print(" | ");
      }
      System.out.println();
      System.out.println("----------------------");
    }
  }
}
