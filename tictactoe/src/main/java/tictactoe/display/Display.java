package tictactoe.display;

public interface Display {
  void display(char[][] board);
}
