package tictactoe.player;

import tictactoe.board.Position;

public class NormalMove implements DifficultyStrategy {

  @Override
  public Position makeMove(char[][] currentBoard) {
    int pcwin = 10000;
    int pcnotlose = 1000;
    int humanOne = 90;
    int pcone = 65;
    int nothing = 11;
    int[][] score = new int[3][3];
    int[][] scoreboard = new int[3][3];


    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (currentBoard[i][j] == 'X') {
          scoreboard[i][j] = 1;
        }
        if (currentBoard[i][j] == 'O') {
          scoreboard[i][j] = 2;
        }
        if (currentBoard[i][j] == '-') {
          scoreboard[i][j] = 0;
        }
      }
    }

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (scoreboard[i][j] != 0) {
          score[i][j] = -100;
        } else {
          //two own pieces on the board; plus 1000000 score 00-; -00; 0-0; case
          //condition one: have two; PC pieces; two: must have 0; condition 3: must have 0;
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 4)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == -1) {
            score[i][j] = score[i][j] + pcwin;
          }
          //same logic check the colomn with the above;
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 4)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == -1) {
            score[i][j] = score[i][j] + pcwin;
          }

          //two pieces human take; XX-;-XX; X-X; same logic that the hunman takes 2 same pieces;
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 2)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 0) {
            score[i][j] = score[i][j] + pcnotlose;
          }

          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 2)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 0) {
            score[i][j] = score[i][j] + pcnotlose;
          }

          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 2)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 1) {
            score[i][j] = score[i][j] + pcone;
          }


          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 2)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 1) {
            score[i][j] = score[i][j] + pcone;
          }

          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 1)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 0) {
            score[i][j] = score[i][j] + humanOne;
          }


          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 1)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 0) {
            score[i][j] = score[i][j] + humanOne;
          }

          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 0)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == -1) {
            score[i][j] = score[i][j] + nothing;
          }
          //
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 0)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == -1) {
            score[i][j] = score[i][j] + nothing;
          }
        }
      }
    }
    int maxi = 0;
    int maxj = 0;
    int temp = 0;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (score[i][j] > temp) {
          temp = score[i][j];
          maxi = i;
          maxj = j;
        }
      }
    }
    return new Position(maxi, maxj);
  }
}

