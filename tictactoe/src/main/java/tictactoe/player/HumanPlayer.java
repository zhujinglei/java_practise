package tictactoe.player;

import java.util.Scanner;

import tictactoe.board.Board;
import tictactoe.board.Piece;
import tictactoe.board.Position;
import tictactoe.display.BoardDisplay;


public class HumanPlayer extends Player {
  int colLoc;
  int rowLoc;

  public HumanPlayer(Piece piece, String name) {
    super(piece, name);
  }


  @Override
  public Position makeMove(char[][] currentBoard) {
    BoardDisplay boardDisplay = new BoardDisplay();
    System.out.println("The current board is");
    boardDisplay.display(currentBoard);
    System.out.println("Your move, " + getName() + ". You are playing as " + getPieces().getStringRepresentation());
    System.out.println("Which location would you like to drop your piece?");
    Scanner reader = new Scanner(System.in);
    reader.useDelimiter("\\D");
    colLoc = reader.nextInt();
    rowLoc = reader.nextInt();
    return new Position(colLoc - 1, rowLoc - 1);
  }
}
