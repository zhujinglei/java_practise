package tictactoe.player;
import tictactoe.board.Piece;
import tictactoe.board.Position;

public abstract class Player {
  private Piece piece;
  private String name;

  public Player(Piece piece, String name){
    this.piece = piece;
    this.name = name;
  }

  public Piece getPieces() {
    return piece;
  }

  public String getName() {
    return name;
  }

  public abstract Position makeMove(char[][] board);
}
