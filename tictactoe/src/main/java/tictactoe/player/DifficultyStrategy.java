package tictactoe.player;

import tictactoe.board.Position;

public interface DifficultyStrategy {
  public Position makeMove(char[][] currentBoard);
}
