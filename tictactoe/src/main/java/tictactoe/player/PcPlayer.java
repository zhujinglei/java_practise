package tictactoe.player;

import tictactoe.board.Piece;
import tictactoe.board.Position;
import tictactoe.display.BoardDisplay;

import javax.print.DocFlavor;

public class PcPlayer extends Player {

  private int difficultylevel;

  public PcPlayer(Piece piece, String name, int difficultylevel) {
    super(piece, name);
    this.difficultylevel = difficultylevel;
  }

  @Override
  public Piece getPieces() {
    return super.getPieces();
  }

  @Override
  public String getName() {
    return super.getName();
  }

  public int getDifficulty() {
    return difficultylevel;
  }

  @Override
  public Position makeMove(char[][] currentBoard) {

    BoardDisplay boardDisplay = new BoardDisplay();
    System.out.println("The current board is");
    boardDisplay.display(currentBoard);
    System.out.println("Your move, " + getName() + ". You are playing as " + getPieces().getStringRepresentation());
    System.out.println("Your current difficulty level is" + getDifficulty());

    if (difficultylevel == 1) {
      SimpleMove simpleMove = new SimpleMove();
      return simpleMove.makeMove(currentBoard);
    }

    if (difficultylevel == 2) {
      NormalMove normalMove = new NormalMove();
      return normalMove.makeMove(currentBoard);
    }

    if (difficultylevel == 3) {
      SmartMove move = new SmartMove();
      return move.makeMove(currentBoard);
    } else {
      return null;
    }
  }
}
