package tictactoe.player;

import tictactoe.board.Position;

public class SimpleMove implements DifficultyStrategy {
  @Override
  public Position makeMove(char[][] currentBoard) {

    int xloc=0;
    int yloc=0;

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (currentBoard[i][j] == '-') {
          xloc = i;
          yloc = j;
        }
      }

    }
    return new Position(xloc, yloc);
  }
}
