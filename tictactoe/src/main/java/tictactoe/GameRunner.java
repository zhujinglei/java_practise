package tictactoe;

import tictactoe.board.BoardUpdate;
import tictactoe.player.Player;
import tictactoe.player.HumanPlayer;
import tictactoe.board.Board;
import tictactoe.analysis.GameState;
import tictactoe.board.Position;

public class GameRunner {

  private Player player1;
  private Player player2;
  private GameConfig gameConfig;

  public GameRunner(Player player1, Player player2, GameConfig gameConfig) {
    this.player1 = player1;
    this.player2 = player2;
    this.gameConfig = gameConfig;
  }

  public void play() {

    Board gameBoard = new Board(gameConfig);

    char[][] board = gameBoard.initBoard();

    Player activePlayer = player1;

    Player passivePlayer = player2;

    GameState gameState = new GameState();
    BoardUpdate boardUpdate = new BoardUpdate();
    boolean currentStateisfinish = false;

    while (!currentStateisfinish) {

      Position currentMove = activePlayer.makeMove(board);

      char[][] updatedboard = boardUpdate.update(board, activePlayer.getPieces().getStringRepresentation(), currentMove);

      currentStateisfinish = gameState.isFinish(updatedboard);

      Player passive = passivePlayer;
      passivePlayer = activePlayer;
      activePlayer = passive;
    }
    System.out.println("Game finish! Enjoy!");

  }


}
