package tictactoe.board;

import tictactoe.board.Position;


public class BoardUpdate {
  char[][] board;
  char sign;
  Position position;


  public char[][] update(char[][] board, char sign, Position position) {

    board[position.getX()][position.getY()] = sign;
    return board;

  }

}
