package tictactoe.board;

public enum Piece {
  X('X'),
  O('O');

  private char stringRepresentation;

  Piece(char stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }

  public char getStringRepresentation() {
    return stringRepresentation;
  }

  public Piece getOther() {
    if(this == X) {
      return O;
    } else {
      return X;
    }
  }
}
