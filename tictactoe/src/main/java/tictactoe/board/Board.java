package tictactoe.board;


import tictactoe.GameConfig;

public class Board {

  private GameConfig config;


  public Board(GameConfig config) {
    this.config = config;

  }

  public GameConfig getConfig() {
    return config;
  }

  public char[][] initBoard() {
    int height = config.getHeight();
    int width = config.getWidth();
    char[][] board = new char[height][width];
    for (int i = 0; i <= height - 1; i++) {
      for (int m = 0; m <= width - 1; m++) {
        board[i][m] = '-';
      }
    }
    return board;
  }
}
