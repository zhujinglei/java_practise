package tictactoe;

import tictactoe.board.Board;
import tictactoe.board.Piece;
import tictactoe.player.HumanPlayer;
import tictactoe.player.PcPlayer;

public class Game {
  public static void main(String[] args) {
    GameRunner game = new GameRunner(new HumanPlayer(Piece.O, "Player 1"),
        new PcPlayer(Piece.X, "Player 2",2), new GameConfig(3, 3));
    game.play();
  }
}
