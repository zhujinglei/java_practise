// Dynamic Initialization; the variable can be initialized during run time;

// a block defines a scope;


public class DynInt {
    public static void main(String args[]){
        double radius = 4, height = 5 ;

        double volume = 3.14 *radius *height;

        System.out.println("The Volume is " + volume);
    }

}

