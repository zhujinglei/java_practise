
//

import java.util.Scanner;


public class GalToLit {

    public static void main(String args[]){
        Scanner reader  = new Scanner(System.in);
        System.out.println("Please enter the amount of gallons you want to convert to liters : ");
        double liters;
        double gallons = reader.nextDouble();

        reader.close();

        liters = gallons * 3.7854;

        System.out.println(gallons + " gallons is " + liters + " liters.");

    }
}


