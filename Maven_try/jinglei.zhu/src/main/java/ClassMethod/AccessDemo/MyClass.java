package ClassMethod.AccessDemo;

class MyClass {

    private int alpha;

    int beta;

    int gamma;


    MyClass(int a, int b, int g){
        this.alpha= a;
        this.beta = b;
        this.gamma = g;
    }

    void setAlpha(int a){
        alpha = a;

    }

    int getAlpha(){
        return alpha;
    }

}
