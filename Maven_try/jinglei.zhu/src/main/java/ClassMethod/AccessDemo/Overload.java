package ClassMethod.AccessDemo;

public class Overload {

    void ovlDemo(){
        System.out.println("No parameters");
    }

    void ovlDemo(int a){
        System.out.println("One int parameter: " + a );
    }

    void ovlDemo (int a, int b){
        System.out.println(" Two parameter: " + a + " " +b);
    }

    double ovlDemo(double a, double b) {
        System.out.println("Two double parameters: " + a + " " + b);
        return a+b;
    }
}
