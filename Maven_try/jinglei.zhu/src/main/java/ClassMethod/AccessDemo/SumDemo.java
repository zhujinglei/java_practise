package ClassMethod.AccessDemo;

public class SumDemo {
    public static void main(String args[]){

        Summation S1 = new Summation(5);

        Summation S2 = new Summation(S1);


        System.out.println("S1.sum: " +S1.sum);

        System.out.println("S2.sum: " + S2.sum);
    }
}
