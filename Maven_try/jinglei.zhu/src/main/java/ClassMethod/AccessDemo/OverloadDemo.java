package ClassMethod.AccessDemo;

public class OverloadDemo {

    public static void main (String args[]){
        Overload ob = new Overload();

        int resI;

        double resD;

        ob.ovlDemo();
        System.out.println();

        ob.ovlDemo(2);
        System.out.println();

        resD = ob.ovlDemo(4.2, 6.2);

        System.out.println("Resuld of ob.ovlDemo(4.2,6.2): " + resD);


    }
}


// notice that the constractor can be overload as well;

