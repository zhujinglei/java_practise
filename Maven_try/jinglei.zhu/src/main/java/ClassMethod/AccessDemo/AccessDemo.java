package ClassMethod.AccessDemo;

public class AccessDemo {

    public static void main (String args[]){

        MyClass ob = new MyClass(1,2,4);

        /* Access to alpha is allowed only through it
        asscessor method */

        ob.setAlpha(1000);
        System.out.println(ob.getAlpha());

        ob.beta =100;
        ob.gamma=10000;

    }
}
