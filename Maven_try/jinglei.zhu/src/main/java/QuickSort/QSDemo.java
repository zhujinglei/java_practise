package QuickSort;

public class QSDemo {

    public static void main(String args[]){

        char a[] = {'x', 'd' ,'s', 'd', 'a', 'o'};
        int i;

        System.out.println("Orignal array: ");

        for(char x: a){
            System.out.print(x +"\t");
        }
        System.out.println("****************************************");

        QuickSort.qsort(a);

        System.out.println("This is the sorted array: ");

        for(char x: a){

            System.out.print(x+"\t");
        }

    }
}
