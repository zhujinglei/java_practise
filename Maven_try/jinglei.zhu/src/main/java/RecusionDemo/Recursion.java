package RecusionDemo;

public class Recursion {

    public static void main(String args[]) {

        Factorial f = new Factorial();

        System.out.println("Factorials using recusive method.");
        System.out.println("Factorial of 3 is\t" + f.factr(3));
        System.out.println("Factorial of 4 is\t" + f.factr(4));
        System.out.println("Factorial of 5 is\t" + f.factr(5));
        System.out.println("Factorial of 6 is\t" + f.factr(6));

        System.out.println("Factorials using iterative method.");
        System.out.println("Factorial of 3 is\t" + f.factI(3));
        System.out.println("Factorial of 4 is\t" + f.factI(4));
        System.out.println("Factorial of 5 is\t" + f.factI(5));
        System.out.println("Factorial of 6 is\t" + f.factI(6));
    }


}
