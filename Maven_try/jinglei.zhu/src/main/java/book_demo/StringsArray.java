package book_demo;

public class StringsArray {
    public static void main(String args[]){

        String strs [] = {"This", "is", "a", "test."};

        System.out.println("Orignal array: ");

        for (String s:strs){
            System.out.println(s+ " ");
        }
        System.out.println(("\n"));
    }

}
