/*
Write down a Java program that prints n fibonacci numbers whereas, n is the number of fibonacci numbers to print e.g.,
if a user inputs 3 as an input (value of n), the program should print 1,2,3 as three fibonacci numbers (as 1+2=3).
If a user inputs 4 as value of n the output should be 1,2,3,5 as; 1+2=3 and 2+3=5.
You have to use While loop to implement the program.
* */

package exercise;

public  class FibNum {

     public FibNum(){}

     public static int[] FibArr(int n) {

        int i = 2;
        int[] FibArr = new int[n];

        if (n <= 2) {
            FibArr[0] = 1;
            FibArr[n - 1] = 1;
        } else {
            FibArr[0] = 1;
            FibArr[1] = 1;
            while (i < n) {
                int sum = FibArr[i - 2] + FibArr[i - 1];
                FibArr[i] = sum;
                i++;
            }
        }

        for (int x : FibArr) {
            System.out.print(x + " ");
        }
        return FibArr;
    }

}
