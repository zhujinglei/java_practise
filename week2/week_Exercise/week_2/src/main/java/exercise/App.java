package exercise;

import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Please select the case! 1: Fibbonacci; 2: Integer Palindrome Check; 3: String Palindrome Check" +
                "4: Hourglass Printing");
        Scanner reader = new Scanner(System.in);
        int choose = reader.nextInt();

        switch (choose) {

            case 1:
                System.out.println("Enter a number for fibonacci number: ");
                Scanner reader_fib = new Scanner(System.in);
                int n = reader_fib.nextInt();
                FibNum MyObject = new FibNum();
                MyObject.FibArr(n);
                break;

            case 2:
                System.out.println("Enter a integer for Palindrome Check: ");
                Scanner reader_int = new Scanner(System.in);
                int in = reader_int.nextInt();
                IntPalindrome NewCheck = new IntPalindrome();
                NewCheck.Check(in);
                break;

            case 3:
                System.out.println("Enter a String for Palindrome Check: ");
                Scanner reader_str = new Scanner(System.in);
                String str = reader_str.nextLine();

                StringPalindrome StringCheck = new StringPalindrome();
                StringCheck.Check(str);
                break;
            case 4:
                System.out.println("Enter a integer for HourGlass Shape ");
                Scanner reader_shape = new Scanner(System.in);
                int s = reader_shape.nextInt();
                HourGlass NewShape = new HourGlass();
                NewShape.Shape(s);
                break;
            default:
                System.out.println("Please choose a valid number! 1,2,3,4");

        }

    }
}
