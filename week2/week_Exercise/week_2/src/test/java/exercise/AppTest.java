package exercise;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }


    @Test
     public void FibNumTest2()

    {
        FibNum test=new FibNum();

        int result[] = FibNum.FibArr(2);

        int[] expectedResult = new int[] { 1, 1 };

        Assert.assertArrayEquals( expectedResult, result );

    }

    @Test
    public void FibNumTest5()
    {
        FibNum test5=new FibNum();

        int result5[] = FibNum.FibArr(5);

        int[] expectedResult5 = new int[] { 1, 1, 2, 3, 5 };

        Assert.assertArrayEquals( expectedResult5, result5 );

    }

    @Test
    public void IntPalindromeTest()
    {
        IntPalindrome test =new IntPalindrome();
        boolean expected = test.Check(1001);
        Assert.assertTrue(expected);

    }
    @Test
    public void IntPalindromeTest2()
    {
        IntPalindrome test =new IntPalindrome();
        boolean expected = test.Check(100231);
        Assert.assertFalse(expected);

    }


    @Test
    public void StrPalindromeTest()
    {
        StringPalindrome test =new StringPalindrome();
        boolean expected = test.Check("asdadsasd");
        Assert.assertFalse(expected);

    }

    @Test
    public void StrPalindromeTest2()
    {
        StringPalindrome test =new StringPalindrome();
        boolean expected = test.Check("A Santa at Nasa");
        Assert.assertTrue(expected);
    }

}
