package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.DraughtsBoard;
import com.thehutgroup.accelerator.draughts.Position;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class Pawn extends DraughtsPiece {
    public Pawn(Colour colour) {
        super(colour);
    }

    @Override
    public Set<Position> possibleMoves(Position position, DraughtsBoard board) {
        Set<Position> moves = new HashSet<>();
        Function<Integer, Function<Integer, Integer>> yMover;


        switch (this.getColour()) {
            case BLACK:
                yMover = y -> d -> y - d;
                break;
            case WHITE:
                yMover = y -> d -> y + d;
                break;
            default:
                throw new RuntimeException("invalid colour");
        }

        for(int x = -1; x <= 1; x = x + 2) {
            int potentialX = position.getX() + x;
            Position potentialMove = new Position(potentialX, yMover.apply(position.getY()).apply(1));
            if(board.positionVacant(potentialMove)) {
                moves.add(potentialMove);
            }
        }


        for(int delta = -2; delta <= 2; delta = delta + 4) {
            Position captureCandidateMove = new Position(position.getX() + delta,  yMover.apply(position.getY()).apply(2));
            Position captureCandidateOpponent = new Position(position.getX() + delta / 2,  yMover.apply(position.getY()).apply(1));
            if (board.positionVacant(captureCandidateMove) &&
                    board.positionOccupied(Colour.getOther(this.getColour()), captureCandidateOpponent))
            {
                moves.add(captureCandidateMove);
                for(int alpha = -2; alpha <= 2; alpha = alpha + 4) {
                    Position doubleCapture = new Position(captureCandidateMove.getX() + alpha, yMover.apply(captureCandidateMove.getY()).apply(2));
                    Position caotureCandidateOpponent_2 = new Position(captureCandidateMove.getX() + delta / 2, yMover.apply(captureCandidateMove.getY()).apply(1));
                    if (board.positionVacant(doubleCapture) &&
                            board.positionOccupied(Colour.getOther(this.getColour()), caotureCandidateOpponent_2)) {
                        moves.add(doubleCapture);
                    }
                }

            }
            
        }
        return moves;
    }

}
