package com.thehutgroup.accelerator.draughts;

public enum Colour {
    WHITE,
    BLACK;

    public static Colour getOther(Colour colour) {
        switch (colour) {
            case WHITE:
                return BLACK;
            case BLACK:
                return WHITE;
            default:
                throw new RuntimeException("Invalid colour");
        }
    }
}
