package exceptionhandling;

public class ExcDemo5 {

  public static void main(String args[]) {
    int number[] = {4, 8, 6, 32, 64, 128, 256, 512};
    int denom[] = {2, 0, 4, 4, 0, 8};

    for (int i = 0; i < number.length; i++) {
      try {
        System.out.println(number[i] + " / " + denom[i] + " is " + number[i] / denom[i]);
      } catch (ArithmeticException ari) {
        System.out.println("Can not devided by Zero! ");
      } catch (Throwable thb) {
        System.out.println("Some exception occurred!");
      }
    }

  }
}
