package exceptionhandling;

public class NonIntResultException extends Exception {
  int n;
  int d;

  NonIntResultException(int i, int j){
    n=i;
    d=j;
  }
  public String toString(){
    return "Result of " + n + " / " + " is non-integer.";
  }

}

class CustomExceptDemo {
  public static void main(String args[]){

    int nums[] = {4,8,15,32,64,127,256,512};

    int denom[] = {2,0,4,4,0,8};

    for(int i=0; i<nums.length; i++){
      try{
        if((nums[i]%2) !=0)
          throw new NonIntResultException(nums[i],denom[i]);
        System.out.println(nums[i] + "/ " +denom[i] + " is " + nums[i]/denom[i]);
      } catch (ArithmeticException exc){
        System.out.println("Can't divide by Zero!");
      } catch (ArrayIndexOutOfBoundsException exc){
        System.out.println("No matching element found.");
      } catch (NonIntResultException exc){
        System.out.println(exc);
      }
    }


  }

}
