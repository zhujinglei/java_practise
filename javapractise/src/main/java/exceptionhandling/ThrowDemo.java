package exceptionhandling;

// unlike the previous example which capture exception automatically by JVM; It is possible to mannuallu throw
// an exception by using the throw statement. you can also rethrow the excpetion;

class ThrowDemo {
  static void genException() {
    int nums[] = new int[4];
    System.out.println(" Before the exception is generated.");
    nums[7] = 10;
    System.out.println(" This wont be displayed");

  }
  }



class UseThrowableMethod{
    public static void main (String args[]){
      try{
        ThrowDemo.genException();
      } catch (ArrayIndexOutOfBoundsException exc){
        System.out.println("Standard message is: ");
        System.out.println(exc);
        System.out.println("\nStack trace:");
        exc.printStackTrace();

      }
      System.out.println("After catching statement! ");

    }

  }

