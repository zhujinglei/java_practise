package com.thg.accelerator;

// this is a simple generic class; T is the type of parameter that will be replaced by a real type; when
// an objet of SimpleGeneric is created;
public class SimpleGeneric<T> {

  T ob; // declare an object of type T;

  SimpleGeneric(T o) {
     ob = o;
  }

  void showType(){
    System.out.println("Type of T is " + ob.getClass().getName());
  }
}
