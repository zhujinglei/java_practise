package com.thg.accelerator;

public class LambdaDemo {
  public static void main(String[] args) {
    MyValue myValue; // declare an interface reference;

    // here, the lambda expression is simply a constant expression;
    // When it is assigned to myValue, a class instance is;
    // constructed in which the lambda expression implements;
    // the getValue() method in MyValue

    myValue = () -> 98.6;
    // call getValue(), which the lambda expression is simply a constant expression;
    // when it is assignede to myValue the lambda expression implements;
    // the getValue method in Myvalue;
  }
}
