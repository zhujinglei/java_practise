package enumeration;

//Enum cannot inherit another; class; Secommd enum cannot be a super class; this means an enum cannot be extended;

enum Transport2 {

  CAR(100,4), TRUCK(55,100), AIRPLANE(600,900), TRAIN(70,100), BOAT(22,1);

  private int speed;
  private int people;
  Transport2(int s,int p) {
    speed = s;
    people = p;
  }

  int getSpeed() {
    return speed;
  }

  public int getPeople() {
    return people;
  }
}

public class EnumDemo3 {
  public static void main(String args[]) {
    System.out.println(" Typical speed for an airplane is " + Transport2.AIRPLANE.getSpeed()+ " miles per hour");
    System.out.println(" All transport speeds: ");
    for(Transport2 t2 : Transport2.values()){
      System.out.println( t2 + " typical speed os " + t2.getSpeed() +  " miles per hour! ");
    }
  }
}
