package enumeration;

enum Transport {
  CAR, TRUCK, AIRPLANE, TRAIN, BOAT
}


public class EnumDemo {
  public static void main(String args[]) {

    Transport tp; // declare a Traisnport varieties.

    tp = Transport.AIRPLANE;

    System.out.println(" The value of the tp is:  " + tp);

    System.out.println("");


    tp = Transport.TRAIN;

    System.out.println(" The value of the tp is: " + tp);

    System.out.println(" ");


    tp =Transport.AIRPLANE;
    //compare two enum;

    if (tp == Transport.TRAIN) {
      System.out.println(" tp contains Train. \n");
    }


    // USE an enum to control a switch statement;

    switch (tp) {
      case CAR:
        System.out.println("A car carries people.");
        break;
      case BOAT:
        System.out.println("A boat sails on water.");
        break;
      case TRAIN:
        System.out.println("A train runs on rails.");
        break;
      case AIRPLANE:
        System.out.println("An airplane flies.");

    }


  }

}
