package enumeration;

public class EnumDemo4 {

  public static void main(String args[]){

    Transport tp1,tp2,tp3;

    System.out.println(" Here are all transport constants " + " and their ordinal values: ");

    for(Transport t: Transport.values())
      System.out.println(t + " " + t.ordinal()); // obtain the ordinal values;

    tp1 = Transport.AIRPLANE;
    tp2 = Transport.TRAIN;
    tp3 = Transport.BOAT;


    // the compare to is the commend that compare the order!
    if(tp1.compareTo(tp2)<0){
      System.out.println( tp1 + " comes before " + tp2 );
    }
  }
}
