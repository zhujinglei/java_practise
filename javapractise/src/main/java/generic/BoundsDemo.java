package generic;


class NumericFns<T extends Number>{
  T num;

  public NumericFns(T num) {
    this.num = num;
  }

  double reciprocal(){
    return 1/num.doubleValue();

  }
  double fraction(){
    return num.doubleValue() - num.intValue();
  }

  boolean absEqual(NumericFns<?> ob) {
    if (Math.abs(num.doubleValue()) == Math.abs(ob.num.doubleValue()))
      return true;
    return false;
  }
}

public class BoundsDemo {

  public static void main(String args[]){

    NumericFns numericFns = new NumericFns<Integer>(5);

    System.out.println("The generated example: " + numericFns.fraction());

    System.out.println("The generated example 2: " + numericFns.reciprocal());


  }
}
