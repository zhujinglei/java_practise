package generic;

//Wildcard arguments can be bounded in much the same way that a type parameter can be bounded. A bounded wildcard is especially
// important when you are creating a method that is designed to operate only on objects that are subclass pf a special superclass/
// <? extends subclass>
// <? super class>
public class BoundedWildcard {

}
