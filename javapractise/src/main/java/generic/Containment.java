package generic;

// A generic interface example; this interface implies that an implementating;
// class contains one or more values;

public interface Containment<T> {

  boolean contains(T o);
}
