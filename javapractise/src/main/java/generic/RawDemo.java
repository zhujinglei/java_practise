package generic;

class Genrow<T>{
  T ob;

  public Genrow(T ob) {
    this.ob = ob;
  }

  public T getOb() {
    return ob;
  }
}

public class RawDemo {
  public static void main(String args[]){

    Genrow<Integer> obj = new Genrow<Integer>(88);
    Genrow<String> sobj = new Genrow<String>("hello world");

  }
}
