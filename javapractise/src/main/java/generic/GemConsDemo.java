package generic;

// A constructor can be generic even if its class is not!

class Simmation {
  private int sum;

  public <T extends Number >Simmation(T args) {
    sum =0;

    for(int i=0; i<=args.intValue(); i++){

      sum +=i;
    }
  }

  int getSum(){
    return sum;
  }
}

public class GemConsDemo {
  public static void main(String args[]){
    Simmation ob = new Simmation(4.0);
    System.out.println(" the summation of the number is " + ob.getSum());
  }
}
