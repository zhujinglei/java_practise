package generic;

// As useful as type safety

class NumericFNS<T extends Number>{
  T num;

  public NumericFNS(T num) {
    this.num = num;
  }

  double reciprocal(){
    return 1/num.doubleValue();
  }

  double fraction(){
    return num.doubleValue()-num.intValue();
  }

  boolean absEqual(NumericFNS<?> ob){
    if(Math.abs(num.doubleValue())==Math.abs(ob.num.doubleValue()))
      return true;
    return false;
  }
}


public class WildcardDemo {
  public static void main(String args[]){

    NumericFNS<Integer> iob= new NumericFNS<Integer>(6);
    NumericFNS<Double> dob = new NumericFNS<Double>(-6.0);
    NumericFNS<Long> lob = new NumericFNS<Long>(5L);

    if(iob.absEqual(dob)){
      System.out.println(" Absolute values are equal");
    }
    else {
      System.out.println("Absolute values are not equal");
    }


  }
}
