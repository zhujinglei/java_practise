package generic;

import java.util.Comparator;

//method inside a generic class can use one or more type parameter;
// it is possible to create a generic method that is enclosed within a nongeneric class;
// as declared in the following example;

public class GenericMethod {

  static <T extends Comparable<T>, V extends T> boolean arrayEqual(T[] x, V[] y) {
    // the array lenth is different then the lenth of the arrat is different, then
    // two array are different;
    if (x.length != y.length) {
      return false;
    }
    for (int i = 0; i < x.length; i++) {
      if(!x[i].equals(y[i])) return false;
    }
    return true;
  }
// A class that implements Comparable defines objects that are capable of being compared;

  public static void main(String args[]){
    Integer nums[] = {1,2,3,4,5,6,7};
    Integer nums1[] = {1,2,3,4,5,6,7};
    Integer nums2[] = {1,2,3,4,5,4,7};
    Integer nums3[] = {1,2,3,4,5,6,7,8};

    if(arrayEqual(nums,nums1)){
      System.out.println(" num and nums 1 array are same! ");
    }

    if(arrayEqual(nums,nums2)){
      System.out.println(" num and nums 2 array are same! ");
    }

    if(arrayEqual(nums,nums3)){
      System.out.println(" num and nums 3 array are same! ");
    }

  }

}

