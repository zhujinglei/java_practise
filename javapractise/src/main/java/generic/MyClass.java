package generic;

public class MyClass<T> implements Containment<T> {
  T[] arrayRef;


  public MyClass(T[] arrayRef) {
    this.arrayRef = arrayRef;
  }



  @Override
  public boolean contains(T o) {
    for( T x: arrayRef){
      if(x.equals(o)) return true;
    }return false;
  }
}



class GenIFDemo{
  public static void main(String args[]){
    Integer x[] ={1,2,3,4,5,6};

    MyClass<Integer> ob = new MyClass<Integer>(x);

    if(ob.contains(2)){
      System.out.println("2 is in ob");
    } else System.out.println(" 2 is not in the ob");

  }

}
