package generic;

class TwoGen<T,V>{
  T ob1;
  V ob2;

  public TwoGen(T ob1, V ob2) {
    this.ob1 = ob1;
    this.ob2 = ob2;
  }

  public T getOb1() {
    return ob1;
  }

  public V getOb2() {
    return ob2;
  }

  public void showTypes(){
    System.out.println("Type of T is " + ob1.getClass().getName());
    System.out.println("Type of V is " + ob2.getClass().getName());
  }
}


public class SimpGen {
  public static void main (String args[]){

    TwoGen<Integer,String> tgobj = new TwoGen< Integer, String>(88,"Generic");

    System.out.println("The class type of the generic are : ");
    tgobj.showTypes();

    System.out.println("The first Object in the generic is: " + tgobj.ob1);

    System.out.println("The Second Object in the generic is: " + tgobj.ob2);
  }
}

// the generic example! how to get the generic example for the list?
//