package generic;

// here T is the name of a type parameter; this name used as a place holder for the actual type that
// will be passed to Gen T is the tradition!
class Gen<T> {
  T ob; //declare an object of type T;
  //Pass the constructor a reference to;
  // an object of type T;

// this is the constructor!;
  // notice here in the constructor the o is the type of the T;

  Gen(T o) {
    ob = o;
  }


  // the generic the method;
  T getOb() {
    return ob;
  }
  //getClass().getName() which return a string representation of the class name!;

  void showType() {
    System.out.println(" Type of T is " + ob.getClass().getName());
  }
}


public class GenDemo {
  public static void main(String args[]) {
    //create an genri object;
    Gen<Integer> iOb;
    //create a Gen<Integer> object and assign its//
    //reference to iOb. Notice the use of autoboxing
    // to encapsulate the value 88 within an integer  object
    iOb = new Gen<Integer>(88);
    iOb.showType();
    //Get the value in iOb. Notice that
    // no cast is needed;
    int v = iOb.getOb();
    System.out.println(" Value is " + v);
    // Create a Gen object for Strings;
    Gen<String> strOb = new Gen<String>("Generic Test");
    strOb.showType();
  }

}

// Generic Work only with Reference Type; Primitive data type cannot used for declaration;