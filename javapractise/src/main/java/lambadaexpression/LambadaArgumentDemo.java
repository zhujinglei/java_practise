package lambadaexpression;

public class LambadaArgumentDemo {
  static String changeStr(StringFunc sf, String s) {
    return sf.func(s);
  }


  public static void main(String args[]) {

    String inStr = "Lambada expressions Expand Java";
    String outStr;
    System.out.println("Here is the input string" + inStr);
    StringFunc stringFunc = (str) -> {
      String result = " ";
      for (int i = str.length() - 1; i >= 0; i--) {
        result += str.charAt(i);
      }
      return result;
    };
    outStr = changeStr(stringFunc, inStr);
    System.out.println("The string reversed: " + outStr);
  }
}



