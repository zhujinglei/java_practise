package lambadaexpression;

public interface NumericFunc<T> {

  int func(T o);

}
