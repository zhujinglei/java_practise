package lambadaexpression;

public interface IntPredicate {
  boolean test(int n);
}
