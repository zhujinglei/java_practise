package lambadaexpression;

public interface NumericTest {
  boolean test(int n, int m);
}
