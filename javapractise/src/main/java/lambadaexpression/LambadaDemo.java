package lambadaexpression;

public class LambadaDemo {
  public static void main(String args[]){
    MyValue myValue; //declare an interface reference

    // Here, the lambada expression is simply a constant expression;
    //when it is assigned to myValue, a class instance is constructed in which the lambada expression implements;
    // the getValue() method in my value;

    myValue = () ->98.6;
    System.out.println(" A constant value: " + myValue.getvalue());
    // create a parameterized lambada expression and assign it to a new MyParameter reference; This lambada expression
    // returns the reciprocal pof its argument;
    // a lambada expression has a parameter;
    MyParamValue myParamValue =(n) ->1.0/n;

    System.out.println("Reciprocal of 4 is: " + myParamValue.getValue(4.0));
    System.out.println("Reciprocal of 8 is:  " + myParamValue.getValue(8.0));

  }
}
