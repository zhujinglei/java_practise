package lambadaexpression;

public interface StringFunc {
  String func(String str);
}
