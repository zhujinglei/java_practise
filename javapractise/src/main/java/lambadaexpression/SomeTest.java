package lambadaexpression;

public interface SomeTest<T> {
  boolean test(T n, T m);
}
