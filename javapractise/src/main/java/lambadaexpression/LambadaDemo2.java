package lambadaexpression;

public class LambadaDemo2 {
  public static void main(String args[]){

    NumericTest numericTest =(n,d) ->(n%d) ==0;

    if(numericTest.test(10,2))
      System.out.println("2 is not factor of 10");

  }
}
