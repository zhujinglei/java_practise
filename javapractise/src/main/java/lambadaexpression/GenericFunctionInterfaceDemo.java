package lambadaexpression;

public class GenericFunctionInterfaceDemo {

  public static void main(String args[]) {
    SomeTest<Integer> someTestInt = (n, d) -> (n % d) == 0;

    if (someTestInt.test(10, 2)) {
      System.out.println("2 is a factor of 10");

    SomeTest<String> stringSomeTest = (a, b) -> a.indexOf(b) != -1;

    String str=" Generic Functional Interface";

    System.out.println("Testing string: " +str);

    if(stringSomeTest.test(str,"face")){
      System.out.println(" 'face' is found. ");
    }else {
      System.out.println(" 'face' is not found! ");
    }
    }
  }


}
