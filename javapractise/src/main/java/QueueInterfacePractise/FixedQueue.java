package QueueInterfacePractise;

public class FixedQueue implements ICharQ {

  private char q[];

  private int putloc;
  private int getloc;

  public FixedQueue(int size) {
    q = new char[size];
    putloc = getloc=0;
  }

  @Override
  public void put(char ch) {

    if(putloc ==q.length){
      System.out.println("The Que is full already;");
      return;
    }else {
      q[putloc] =ch;
      putloc +=1;
    }

  }

  @Override
  public char get() {
    if(getloc==putloc){
      System.out.println("The queue is empty!");
      return (char) 0;
    } else {
      char ch= q[getloc];
      getloc +=1;
      return ch;
    }
  }
}
