package QueueInterfacePractise;

public interface ICharQ {

  public void put(char ch);

  public char get();

}
