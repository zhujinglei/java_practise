package QueueInterfacePractise;

public class DefaultMethodDemo {
  public static void main (String args[]){

    MyIFImp obj = new MyIFImp();

    System.out.println("User ID is " + obj.getUserID());

    System.out.println("The default admin ID is " + obj.getAdminID());

  }
}
