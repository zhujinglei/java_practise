package exercise_3.car_interface;

public class SportsCar extends CarClass {
  double taxation;

  SportsCar(int ch, String t, double p) {

    super(ch, t, p);

  }

  public double taxCalculation( double power) {

    if (power <= 1.5) {
      return taxation = 100;

    } else {
      return taxation = 200;

    }
  }

}

