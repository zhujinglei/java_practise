package exercise_3.car_interface;

public class BusinessCar extends CarClass {
  double taxation;

  BusinessCar(int ch, String t, double p) {

    super(ch, t, p);

  }

  public double taxCalculation(double power) {

    if (power <= 1.5) {
      return taxation = 200;

    } else {
      return taxation = 250;

    }
  }

}
