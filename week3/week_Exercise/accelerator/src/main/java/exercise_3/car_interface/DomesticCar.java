package exercise_3.car_interface;

public class DomesticCar extends CarClass {
  double taxation;

  DomesticCar(int ch, String t, double p) {

    super(ch, t, p);

  }

  public double taxCalculation(double power) {

    if (power <= 1.5) {
      return taxation = 20;

    } else {
      return taxation = 90;

    }
  }

}
