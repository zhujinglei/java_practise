package exercise_3.car_interface;

public class CarClass implements Taxation {
  private int chassidouNumber;
  private String type;
  private double power;


  CarClass(int ch, String t, double p) {
    this.chassidouNumber = ch;
    this.type = t;
    this.power = p;
  }


  public int getChassidouNumber() {
    return chassidouNumber;
  }

  public void setChassidouNumber(int chassidouNumber) {
    this.chassidouNumber = chassidouNumber;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public double getPower() {
    return power;
  }

  public void setPower(double power) {
    this.power = power;
  }


  @Override
  public int bodyNumber() {
    return getChassidouNumber();
  }

  @Override
  public String carType() {
    return getType();
  }

  @Override
  public double enginePower() {
    return getPower();
  }

  public double taxCalculation() {
    return 0;
  }
}

