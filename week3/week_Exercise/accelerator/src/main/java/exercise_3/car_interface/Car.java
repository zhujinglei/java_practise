package exercise_3.car_interface;

public interface Car {

  int bodyNumber();

  String carType();

  double enginePower();

}
