package exercise_3.car_interface;

public interface Taxation extends Car {
  double taxCalculation();

}
