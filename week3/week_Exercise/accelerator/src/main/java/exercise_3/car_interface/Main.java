package exercise_3.car_interface;

public class Main {
  public static void main(String args[]) {
    BusinessCar b1 = new BusinessCar(101010, "A1", 3);

    System.out.println("The tax per year is " + b1.taxCalculation(3));

    System.out.println("The info of the vehicle:");

    System.out.println("The chasis number is" + b1.getChassidouNumber());

    System.out.println("The type of the vehicle is " + b1.getType());

    System.out.println("The engine power of the vehicle is " + b1.enginePower());

    SportsCar s1 = new SportsCar(202020, "SP1", 6);

    System.out.println("The tax per year is " + s1.taxCalculation(6));

    System.out.println("The info of the vehicle:");

    System.out.println("The chasis number is" + s1.getChassidouNumber());

    System.out.println("The type of the vehicle is " + s1.getType());

    System.out.println("The engine power of the vehicle is " + s1.enginePower());

    DomesticCar d1 = new DomesticCar(808080, "DT1", 1);
    System.out.println("The tax per year is " + d1.taxCalculation(1));

    System.out.println("The info of the vehicle:");

    System.out.println("The chasis number is" + d1.getChassidouNumber());

    System.out.println("The type of the vehicle is " + d1.getType());

    System.out.println("The engine power of the vehicle is " + b1.enginePower());
  }
}
