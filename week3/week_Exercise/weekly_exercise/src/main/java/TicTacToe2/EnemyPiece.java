package TicTacToe2;

import javax.print.DocFlavor;

public class EnemyPiece extends Piece {

  public EnemyPiece(char sign) {
    super(sign);
  }

  @Override
  public char getPieceown() {
    return super.getPieceown();
  }

  public int[][] scoreboard(char[][] board) {
    int[][] scoreboard = new int[3][3];
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (board[i][j] == 'X') {
          scoreboard[i][j] = 1;
        }
        if (board[i][j] == 'O') {
          scoreboard[i][j] = 2;
        }
        if (board[i][j] == '-') {
          scoreboard[i][j] = 0;
        }
      }
    }
    return scoreboard;
  }

  int[][] pc_move(int[][] scoreboard) {
    int pcwin = 100000;
    int pcnotlose = 10000;
    int humanOne = 100;
    int pcone = 66;
    int nothing = 10;
    int[][] score = new int[3][3];

    // assign the score to the board; if the board has been occupied then assign

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (scoreboard[i][j] != 0) {
          score[i][j] = -100;
        } else {

          //two own pieces on the board; plus 1000000 score 00-; -00; 0-0; case
          //condition one: have two; PC pieces; two: must have 0; condition 3: must have 0;
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 4)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == -1) {
            score[i][j] = score[i][j] + pcwin;
          }
          //same logic check the colomn with the above;
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 4)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == -1) {
            score[i][j] = score[i][j] + pcwin;
          }

          //two pieces human take; XX-;-XX; X-X; same logic that the hunman takes 2 same pieces;
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 2)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 0) {
            score[i][j] = score[i][j] + pcnotlose;
          }
          // column
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 2)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 0) {
            score[i][j] = score[i][j] + pcnotlose;
          }

          // one pieces  pc X--; -X-; --X; (only one) the condition followed; row
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 2)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 1) {
            score[i][j] = score[i][j] + pcone;
          }

          // the column only have one pc; condition similar;
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 2)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 1) {
            score[i][j] = score[i][j] + pcone;
          }

          // the column only have one humna picece; change the 2 to 1; same logic row
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 1)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == 0) {
            score[i][j] = score[i][j] + humanOne;
          }

          // same; have one human piece; colum;
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 1)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == 0) {
            score[i][j] = score[i][j] + humanOne;
          }
          //no piece on the location// nothing on the the row  and column;
          if (((scoreboard[0][j] + scoreboard[1][j] + scoreboard[2][j]) == 0)
              && (scoreboard[0][j] * scoreboard[1][j] * scoreboard[2][j]) == 0
              && ((scoreboard[0][j] - 1) * (scoreboard[1][j] - 1) * (scoreboard[2][j] - 1)) == -1) {
            score[i][j] = score[i][j] + nothing;
          }
          //
          if (((scoreboard[i][0] + scoreboard[i][1] + scoreboard[i][2]) == 0)
              && (scoreboard[i][0] * scoreboard[i][1] * scoreboard[i][2]) == 0
              && ((scoreboard[i][0] - 1) * (scoreboard[i][1] - 1) * (scoreboard[i][2] - 1)) == -1) {
            score[i][j] = score[i][j] + nothing;
          }
          // diagonal situation; the first condition is position (0,0);(1,1)(2,2);
          //
          // apply same logic; with the colum or row; pc;
          if ((i == 0 && j == 0) || (i == 1 && j == 1) || (i == 2 && j == 2)) {
            if (((scoreboard[0][0] + scoreboard[1][1] + scoreboard[2][2]) == 4)
                && (scoreboard[0][0] * scoreboard[1][1] * scoreboard[2][2]) == 0
                && ((scoreboard[0][0] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][2] - 1)) == -1) {
              score[i][j] = score[i][j] + pcwin;
            }
            //human case; has two piece on the diaganol;
            if (((scoreboard[0][0] + scoreboard[1][1] + scoreboard[2][2]) == 2)
                && (scoreboard[0][0] * scoreboard[1][1] * scoreboard[2][2]) == 0
                && ((scoreboard[0][0] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][2] - 1)) == 0) {
              score[i][j] = score[i][j] + pcnotlose;
            }

            //human has only one pieces;
            if (((scoreboard[0][0] + scoreboard[1][1] + scoreboard[2][2]) == 1)
                && (scoreboard[0][0] * scoreboard[1][1] * scoreboard[2][2]) == 0
                && ((scoreboard[0][0] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][2] - 1)) == 0) {
              score[i][j] = score[i][j] + humanOne;
            }

            //pc has only one pieces;
            if (((scoreboard[0][0] + scoreboard[1][1] + scoreboard[2][2]) == 2)
                && (scoreboard[0][0] * scoreboard[1][1] * scoreboard[2][2]) == 0
                && ((scoreboard[0][0] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][2] - 1)) == 1) {
              score[i][j] = score[i][j] + pcone;
            }

            // both have no on the diagonal;
            if (((scoreboard[0][0] + scoreboard[1][1] + scoreboard[2][2]) == 0)
                && (scoreboard[0][0] * scoreboard[1][1] * scoreboard[2][2]) == 0
                && ((scoreboard[0][0] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][2] - 1)) == -1) {
              score[i][j] = score[i][j] + nothing;
            }

          }
          //same logic on the right diaganal;
          if ((i == 0 && j == 2) || (i == 2 && j == 0) || (i == 1 && j == 1)) {
            if (((scoreboard[0][2] + scoreboard[1][1] + scoreboard[2][0]) == 4)
                && (scoreboard[0][2] * scoreboard[1][1] * scoreboard[2][0]) == 0
                && ((scoreboard[0][2] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][0] - 1)) == -1) {
              score[i][j] = score[i][j] + pcwin;
            }

            if (((scoreboard[0][2] + scoreboard[1][1] + scoreboard[2][0]) == 2)
                && (scoreboard[0][2] * scoreboard[1][1] * scoreboard[2][0]) == 0
                && ((scoreboard[0][2] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][0] - 1)) == 0) {
              score[i][j] = score[i][j] + pcnotlose;
            }

            if (((scoreboard[0][2] + scoreboard[1][1] + scoreboard[2][0]) == 1)
                && (scoreboard[0][2] * scoreboard[1][1] * scoreboard[2][0]) == 0
                && ((scoreboard[0][2] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][0] - 1)) == 0) {
              score[i][j] = score[i][j] + humanOne;
            }
            if (((scoreboard[0][2] + scoreboard[1][1] + scoreboard[2][0]) == 2)
                && (scoreboard[0][2] * scoreboard[1][1] * scoreboard[2][0]) == 0
                && ((scoreboard[0][2] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][0] - 1)) == 1) {
              score[i][j] = score[i][j] + pcone;
            }
            if (((scoreboard[0][2] + scoreboard[1][1] + scoreboard[2][0]) == 0)
                && (scoreboard[0][2] * scoreboard[1][1] * scoreboard[2][0]) == 0
                && ((scoreboard[0][2] - 1) * (scoreboard[1][1] - 1) * (scoreboard[2][0] - 1)) == -1) {
              score[i][j] = score[i][j] + nothing;
            }

          }
        }
      }
    }
    return score;

  }
  // the strategy is to find the maximum score of case;
  char[][] pc_stratgy(char[][] board, int[][] score) {

    int maxi = 0;
    int maxj = 0;
    int temp = 0;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (score[i][j] > temp) {
          temp = score[i][j];
          maxi = i;
          maxj = j;
        }

      }
      board[maxi][maxj] = 'O';
    }
    return board;
  }
}