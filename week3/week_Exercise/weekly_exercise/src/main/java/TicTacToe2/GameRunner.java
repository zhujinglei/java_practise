package TicTacToe2;

import java.util.Scanner;

import java.util.Random;


public class GameRunner {

  public static void main(String[] args) {

    System.out.println("Lets start play the game");

    boolean gamefinish = false;
    //boolean notoccupied = false;
    Board newgame = new Board();
    Check newcheck = new Check();

    char[][] newboard = newgame.init_board();

    gamefinish = newcheck.checkwin(newboard);

    newgame.print_board(newboard);

    Piece me = new Piece('X');

    Piece pc = new EnemyPiece('O');

    System.out.println("My sign is " + me.getPieceown());

    System.out.println("The enemy sign is " + pc.getPieceown());
    System.out.println("To be fair lets random generate the number and decide who go first, "
        + "if the the number over 24 you go first!");
    Random rand = new Random();
    int n = rand.nextInt(50);

    if (n > 24) {
      System.out.println("Oh you go first and your sign is X");
      System.out.println("********************************************");
    } else {
      System.out.println("Oh the PC go first and the PC's sign s O");
      System.out.println("********************************************");
      newboard[0][0] = '0';
      newgame.print_board(newboard);
    }

    while (!gamefinish) {

      int a;
      int b;

      System.out.println("********************************************");
      System.out.println("Please input the location of your move");
      Scanner reader = new Scanner(System.in);
      reader.useDelimiter("\\D");
      a = reader.nextInt();
      b = reader.nextInt();

      newboard = me.set_loc(newboard, a, b, me.getPieceown());
      newgame.print_board(newboard);
      System.out.println();
      gamefinish = newcheck.checkwin(newboard);

      int[][] scoreboard = ((EnemyPiece) pc).scoreboard(newboard);
      int[][] score = ((EnemyPiece) pc).pc_move(scoreboard);
      newboard = ((EnemyPiece) pc).pc_stratgy(newboard, score);
      newgame.print_board(newboard);
      gamefinish = newcheck.checkwin(newboard);
      System.out.println();
    }
  }
}


