package TicTacToe2;

public class Board {

  char[][] board = new char[3][3];

  char[][] init_board() {

    for (int i = 0; i <= 2; i++) {
      for (int m = 0; m <= 2; m++) {
        board[i][m] = '-';
      }
    }
    return board;
  }


  void print_board(char[][] board) {
    for (int i = 0; i <= 2; i++) {
      System.out.println("----------------------");
      for (int m = 0; m <= 2; m++) {
        System.out.print(" | ");
        System.out.print(board[i][m]);
        System.out.print(" | ");
      }
      System.out.println();
      System.out.println("----------------------");
    }

  }
}
