package TicTacToe2;

public class Piece {

  private char pieceown;

  Piece(char sign) {
    this.pieceown = sign;
  }


  public char getPieceown() {
    return pieceown;
  }


  char[][] set_loc(char[][] board, int a, int b, char sign) {

    board[(a - 1)][(b - 1)] = sign;

    return board;

  }
}
