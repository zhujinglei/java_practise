package jinglei;


public class CarClass implements Taxation {

  int chasisnum;
  String type;
  double power;
  double tax;

  CarClass(int chasisnum, String type, double power) {
    this.chasisnum = chasisnum;
    this.type = type;
    this.power = power;
  }

  @Override
  public int chasisdouNumberl() {
    System.out.println("The chasisdou Number is " + chasisnum);
    return chasisnum;
  }

  public String type() {
    System.out.println("The type of the vehicle is : " + type);
    return type;
  }

  public double enginepower() {

    System.out.println("The engine power of the vehicle is " + power);

    return power;
  }

  public double taxCalculation() {
    System.out.println(power);
    System.out.println("The taxation of this vehicle is " + tax);
    return tax;
  }
}
