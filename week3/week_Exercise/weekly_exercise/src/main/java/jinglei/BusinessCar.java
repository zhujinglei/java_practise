package jinglei;

public class BusinessCar extends CarClass {

  BusinessCar(int c, String t, double p) {
    super(c, t, p);
  }

  public double taxCalculation() {
    System.out.println("The tax of this vehicle is");
    if (power <= 1.5) {
      System.out.println(200);
      return 20;
    } else {
      System.out.println(220);
      return 90;
    }
  }
}
