package jinglei;

public class DomesticCar extends CarClass {

  DomesticCar(int c, String t, double p) {
    super(c, t, p);
  }

  public double taxCalculation() {
    System.out.println("The tax of this vehicle is");
    if (power <= 1.5) {
      System.out.println(20);
      return 20;
    } else {
      System.out.println(90);
      return 90;
    }
  }

}
