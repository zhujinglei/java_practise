package tic_tac_toe;

public class PlayGame implements Move, CheckOccupied {

  @Override
  public char[][] next_loc(int a, int b) {
    return new char[a][b];
  }

  @Override
  public boolean checkoccuppied(int a, int b) {
    return false;
  }
}
