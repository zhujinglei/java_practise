package tic_tac_toe;

public abstract class GamePhases {

  abstract char[][] initialize();

  abstract void startPlay(char[][] board, char currentpiece);

  abstract void endPlay();


//  public final void play() {
//    initialize();
//    startPlay();
//    endPlay();
//  }
}

