package tic_tac_toe;

import java.util.Scanner;


public class Game extends GamePhases {


  @Override
  char[][] initialize() {

    System.out.println("Lets Start new game");

    Board newboard = new Board();
    char[][] initboard = newboard.init_board();
    newboard.print_board();
    return initboard;
  }


  @Override
  void startPlay(char[][] initboard, char currentpiece) {

    System.out.println("Please input the location of your move");
    PlayGame movement = new PlayGame();
    Scanner reader = new Scanner(System.in);
    reader.useDelimiter("\\D");
    int a;
    int c;
    a = reader.nextInt();
    c = reader.nextInt();
    System.out.println("the location you enter is ( " + a + " , " + c + " )");
    movement.next_loc(a, c);


    if (initboard[a][c] == '-') {
      initboard[a][c] = currentpiece;
    } else {
      System.out.println("Please re-enter the location");
    }
  }

  @Override
  void endPlay() {
    System.out.print("Wow");
  }
}