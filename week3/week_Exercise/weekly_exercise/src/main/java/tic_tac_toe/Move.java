package tic_tac_toe;

public interface Move {
  char[][] next_loc(int a, int b);

}
