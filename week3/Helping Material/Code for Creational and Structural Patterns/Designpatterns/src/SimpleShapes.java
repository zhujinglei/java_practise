public interface SimpleShapes {

    void draw();

    void resize();

    String description();

    boolean isHide();
}
