public class Circle implements SimpleShapes {

    @Override
    public void draw() {
        System.out.println("Circle has been drawn");

    }

    @Override
    public void resize() {
        System.out.println("Circle Resize");

    }

    @Override
    public String description() {
        return "Circle";
    }

    @Override
    public boolean isHide() {
        return false;
    }
}
