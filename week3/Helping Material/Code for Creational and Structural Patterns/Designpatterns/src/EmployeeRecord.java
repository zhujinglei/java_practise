import java.util.ArrayList;
import java.util.List;

public class EmployeeRecord extends EmployeeComponent {


    private List<EmployeeComponent> myChildList;
    private String recordName;

    public EmployeeRecord(String name) {

        this.recordName = name;
        myChildList = new ArrayList<EmployeeComponent>();
    }

    public void addEmployee(EmployeeComponent employee) {
        myChildList.add(employee);
    }

    public void removeEmployee(EmployeeComponent employee) {
        myChildList.remove(employee);
    }

    public List<EmployeeComponent> getChildren() {
        return myChildList;
    }


    public void printDepartmentName() {
        for (int i=0; i<myChildList.size(); i++)
        {
            myChildList.get(i).printDepartmentName();
        }
       /* myChildList.forEach(EmployeeComponent::printDepartmentName);*/
    }
}


