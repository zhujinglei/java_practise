public class LengthDemo {

    public static void main(String args[]){

        int list[] = new int[10];
        int nums[] ={1,23,4343};
        int table[][]={
            {1,2,3},
            {232,232},
            {6,7,8,9,10}
        };

        System.out.println("Length of list is " + list.length);
        System.out.println("Length of nums is " + nums.length);
        System.out.println("Length of tables is " +table.length);
        System.out.println("Length of tables[0] is "+ table[0].length);
        System.out.println("Length of tables[1] is " + table[1].length);
        System.out.println("Length of tables[2] is " +table[2].length);

    }
}
