public class Bubble {
    public static void main(String args[]){

        int nums[]= {1, 23, 44343, -990, 26732, 12312, 123213, 2323, 43435, 6656};

        int a, b, t;

        int size = 10;

        System.out.println("Original array is: ");

        for(int i = 0; i< size; i++){
            System.out.println(" " + nums[i]);

        System.out.println();
        }
        //this is bubble rating;
        for( a=1; a< size; a++){
            for( b=size -1; b>=a; b--){
                if(nums[b-1] > nums[b]){
                    //exchange elements
                    t=nums[b-1];
                    nums[b-1]=nums[b];
                    nums[b]=t;
                }
            }
        }
        System.out.println("Sorted array is: ");
        for (int i=0; i< size; i++){
            System.out.println(" " + nums[i]);
        System.out.println();
        }

    }
}
