public class MinMax2 {
    public static void main(String args[]) {
        int nums[] = {1, 23, 44343, -990, 26732, 12312, 123213, 2323, 43435, 6656};
        int min, max;
        min = max = nums[0];
        for (int i = 1; i < 10; i++) {
            if (nums[i] < min) min = nums[i];
            if (nums[i] > max) max = nums[i];
        }

        System.out.println("Min and Max are " + min +'\t'+max);
    }
}
